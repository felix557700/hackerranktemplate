/* HackerRank Template by Vitas Filip */

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedList;

class Solution1
{
	public static void main(String[] argv) 
	{
		InputReader(System.in);

		solve(); 
	}

	static void solve()
	{
//		Stack<Integer> stack = new Stack<Integer>(); //empty, peek, pop, push, search
//		Vector<Integer> vector = new Vector<Integer>(); //add, contains, get, remove, size, capacity
//		ArrayList<Integer> array = new ArrayList<Integer>(10); // not synchronized
//		LinkedList<Integer> list = new LinkedList<Integer>();
//		HashMap<String, Integer> hash = new HashMap<String, Integer>(); //containsKey, containsValue, put, get, remove, size

		/*------ CODE HERE: -------*/	
		
//		int n = readInt();
//		for (int i = 0; i < n; i++) 
//		{
//			
//		}
		
		
//		System.out.println();	
	}
	

	private static InputStream in;
	private static byte[] buf = new byte[10240];
	private static int curChar;
	private static int numChars;
	
	static void InputReader(InputStream in_stream) {
		in = in_stream;
	}

	static int read() {
		if (numChars == -1)
		{
			//throw new InputMismatchException();
			System.out.print("input missmatch");
		}
		if (curChar >= numChars) {
			curChar = 0;
			try {
				numChars = in.read(buf);
			} catch (IOException e) {
				//throw new InputMismatchException();
				System.out.print(e.getStackTrace());
			}
			if (numChars <= 0)
				return -1;
		}
		return buf[curChar++];
	}
	
	static int readInt() {
		int c = read();
		while (isSpaceChar(c))
			c = read();
		int sgn = 1;
		if (c == '-') {
			sgn = -1;
			c = read();
		}
		int res = 0;
		do {
			if (c < '0' || c > '9')
				throw new InputMismatchException();
			res *= 10;
			res += c - '0';
			c = read();
		} while (!isSpaceChar(c));
		return res * sgn;
	}
	
	// same as readInt
	static int readLong() 
	{
		return readInt();
	}

	static String readString() 
	{
		int c = read();
		while (isSpaceChar(c))
			c = read();
		StringBuilder res = new StringBuilder();
		do {
			res.appendCodePoint(c);
			c = read();
		} while (!isSpaceChar(c));
		return res.toString();
	}

	String readStringLine() 
	{
		try
		{
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isEndChar(c));
			return res.toString();
		}
		catch (InputMismatchException exception) 
		{
			System.out.print(exception.getStackTrace());
			return null;
		}
	}
	
	static boolean isSpaceChar(int c) {
		return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
	}
	
	boolean isEndChar(int c) {
		return c == '\n' || c == '\r' || c == -1;
	}

	String next() {
		return readString();
	}

	
	private static PrintWriter out = null;

	static void OutputWriter(OutputStream outputStream) {
		out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
	}

	void OutputWriter(Writer out_stream) {
		out = new PrintWriter(out_stream);
	}

	void print(Object...objects) {
		for (int i = 0; i < objects.length; i++) {
			if (i != 0)
				out.print(' ');
			out.print(objects[i]);
		}
	}

	void printLine(Object...objects) {
		print(objects);
		out.println();
	}

	static void close() {
		out.close();
	}

	static void flush() {
		out.flush();
	}

}
