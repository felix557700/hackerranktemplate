/* HackerRank Template by Vitas Filip */

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;


class StringHelper 
{
	public static final String truncate( String in, int length ) {
		if( in.length() <= length ) {
			return in;
		}
		return in.substring(0, length );
	}
	public static final String truncateLeft(String in, int length ) {
		if( in.length() <= length ) {
			return in;
		}
		return in.substring(in.length() - length );
	}
	public static final String formatDouble(double value ) {
		return truncate("" + value, 4);
	}
	public static final String mid(String in, int pos, int maxlength ) {
		if( pos + maxlength > in.length() ) {
			return in.substring(pos, in.length() );
		} else {
			return in.substring(pos,maxlength + pos);
		}
	}
}


class Parser 
{
	String line;
	char[] linearray;
	int pos;
	int len;
	
	public Parser(String line ) {
		this.line = line;
		this.linearray = line.toCharArray();
		len = linearray.length;
	}
	
	public Parser()
	{
		this.line = null;
		this.linearray = null;
		len = 0;
	}
	
	public void setLine(String line)
	{
		this.line = line;
		this.linearray = line.toCharArray();
		len = linearray.length;
		pos = 0;
	}
	
	public final int eatInt() {
		return eatInt(' ');
	}
	
	
	public final int eatInt(char delimiter) {
		int newpos = pos;
		int value = 0;
		boolean neg = false;
		if( pos >= len ) {
			throw new RuntimeException("pos at end of string");
		}
		if( linearray[pos] == '-' ) {
			neg = true;
			newpos++;
		}
		if( linearray[pos] == '+' ) {
			pos++;
			newpos++;
		}
		char thischar = 0;
		while( newpos < len && ( thischar = linearray[newpos] ) != delimiter && thischar >= '0' && thischar <= '9' ) {
			value = value * 10 + (int)(linearray[newpos] - '0');
			newpos++;
		}
		if( pos == newpos ) {
			throw new RuntimeException("expected int but found [" + linearray[pos] + "] in [" + StringHelper.mid(line, pos, 50) + "]" );
		}
		pos = newpos;
		return neg ? -value : value;
	}
	
	
	public final double eatDouble() {
		double value = 0;
		boolean negative = false;
		boolean afterpoint = false;
		double divider = 1;
		char thischar = 0;
		int oldpos = pos;
		while( pos < len && ( thischar = linearray[pos] ) != ' ' && thischar != 'e' && thischar != '\t' ) {
			if( thischar == '-') {
				negative = true;
			} else if( thischar == '.' ) {
				afterpoint = true;
			} else {
				int thisdigit = thischar - '0';
				value = value * 10 + thisdigit;							
				if( afterpoint ) {
					divider *= 10;
				}
			}
			pos++;
		}
		if( thischar == 'e' ) {
			pos++;
			boolean exponentnegative = false;
			int exponent = 0;
			while( pos < len && ( thischar = linearray[pos] ) != ' ' && thischar != '\t' ) {
				if( thischar == '-') {
					exponentnegative = true;
				} else if( thischar != '+' ) {
					exponent = exponent * 10 + ( thischar - '0' );
				}
				pos++;
			}
			if( exponentnegative ) {
				exponent = -exponent;
			}
			value *= Math.pow(10,exponent);
		}
		if( negative ) {
			value = -value;
		}
		value /= divider;
		if( pos == oldpos ) {
			throw new RuntimeException("expected double but found [" + linearray[pos] + "] in [" + StringHelper.mid(line, pos, 50) + "]" );
		}
		return value;
	}
	
	
	public final void eatWhitespace() {
		while( pos < len && ( linearray[pos] == ' ' || linearray[pos] == '\t' ) ) {
			pos++;
		}
	}
	
	
	public final void eatChar( char target ) {
		if( linearray[pos] != target ) {
			throw new RuntimeException("expected " + target + " but got " + linearray[pos] + " in [" + linearray.toString() + "]" );
		}
		pos++;
	}
	
	public final String eatString(char target) 
	{
		StringBuilder builder = new StringBuilder();
		
		int first_position = pos;
		
		while( pos < len && (linearray[pos] != target && linearray[pos] != ' ' && linearray[pos] != '\n' && linearray[pos] != '\r' ) ) 
		{
			pos++;
		}
		
		return builder.append(linearray, first_position, pos - first_position).toString();
	}	
	
	public final boolean more() {
		return pos < len;
	}
}



//StringTokenizer stringTokenizer = new StringTokenizer(line);
//while( stringTokenizer.hasMoreTokens()) {
//  double value = Double.parseDouble( stringTokenizer.nextToken() );
//  values[n] = value;
//  n++;
//}  

//parser = new Parser(line);
//while(parser.more()) {
//  double value =  parser.eatDouble();
//  parser.eatWhitespace();
//  values[n] = value;
//  n++;
//} 


class InputReader 
{
		private InputStream stream;
		private byte[] buf = new byte[10240];
		private int curChar;
		private int numChars;
		private SpaceCharFilter filter;

		public InputReader(InputStream stream) {
			this.stream = stream;
		}

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = stream.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}
		
		public int readInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
		
		// same as readInt
		public int readLong() 
		{
			return readInt();
		}

		public String readString() 
		{
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public String readStringLine() 
		{
			try
			{
				int c = read();
				while (isSpaceChar(c))
					c = read();
				StringBuilder res = new StringBuilder();
				do {
					res.appendCodePoint(c);
					c = read();
				} while (!isEndChar(c));
				return res.toString();
			}
			catch (InputMismatchException exception) 
			{
				//System.out.print(exception.getStackTrace());
				return null;
			}
		}
		
		public boolean isSpaceChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}
		
		public boolean isEndChar(int c) {
			if (filter != null)
				return filter.isSpaceChar(c);
			return c == '\n' || c == '\r' || c == -1;
		}

		public String next() {
			return readString();
		}

		public interface SpaceCharFilter {
			public boolean isSpaceChar(int ch);
		}
	}


class OutputWriter 
{
		private final PrintWriter writer;

		public OutputWriter(OutputStream outputStream) {
			writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
		}

		public OutputWriter(Writer writer) {
			this.writer = new PrintWriter(writer);
		}

		public void print(Object...objects) {
			for (int i = 0; i < objects.length; i++) {
				if (i != 0)
					writer.print(' ');
				writer.print(objects[i]);
			}
		}

		public void printLine(Object...objects) {
			print(objects);
			writer.println();
		}

		public void close() {
			writer.close();
		}

		public void flush() {
			writer.flush();
		}

}

public class Solution 
{
	public static int[] readIntArray(InputReader in, int size) 
	{
		int[] array = new int[size];
		
		for (int i = 0; i < size; i++)
			array[i] = in.readInt();
		
		return array;
	}
	
	public static void main(String[] argv) throws FileNotFoundException 
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// open input console
		InputReader in = new InputReader(System.in);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// open input file
		// File fin = new File("out.txt");
		// InputStream input = new FileInputStream(fin);
		// InputReader in = new InputReader(input);				
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// open output console
		OutputWriter out = new OutputWriter(System.out);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////


		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// // open output file
		// File fout = new File("out.txt");
		// FileOutputStream fos = new FileOutputStream(fout);
		// OutputWriter out = new OutputWriter(fos);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////



		// SOLVE PROBLEM
		solve(in, out);



		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// int n;
		// // read input
		// for(int i = 0; i < 1_000_000; i++)
		// {
		// 	n = in.readInt();
		// 	//System.out.print(n + " ");
		// }
		////////////////////////////////////////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// print output
		// for(int i = 0; i < 1_000_000; i++)
		// {
		// 	out.print(new Double((Math.random()*1_000_000)).toString() + " ");
		// }
		
		//flush output
		out.flush();
				
		//remember to close the outputstream, at the end
		out.close();
		////////////////////////////////////////////////////////////////////////////////////////////////////////////


		// long start = System.nanoTime();
		// long end = System.nanoTime();
		// System.out.println(TimeUnit.MILLISECONDS.convert(end - start, TimeUnit.NANOSECONDS) + " ms"+ "\n");
	}

	
	public static void solve(InputReader in, OutputWriter out)
	{
//		Stack<Integer> stack = new Stack<Integer>(); //empty, peek, pop, push, search
//		Vector<Integer> vector = new Vector<Integer>(); //add, contains, get, remove, size, capacity
//		ArrayList<Integer> array = new ArrayList<Integer>(10); // not synchronized
//		LinkedList<Integer> list = new LinkedList<Integer>();
//		HashMap<String, Integer> hash = new HashMap<String, Integer>(); //containsKey, containsValue, put, get, remove, size

		/*------ CODE HERE: -------*/	
		
//		int n = readInt();
//		for (int i = 0; i < n; i++) 
//		{
//			
//		}
		
		
		
//		System.out.println();	
		
	}
}




// methods
//int n = in.readInt();
//String s = in.readString();
//String line = in.readStringLine();
//int[] x = IOUtils.readIntArray(in, 5);


// read double
//String s;
//Parser p = new Parser();
//for(int i = 0; i < 1_000_000; i++)
//{
//	s = in.readString();
//	p.setLine(s);
//	n = p.eatDouble();
//}


// formatiran input(int, String, double): readStringLine() + parser
//String s = in.readStringLine();
//if (s == null) break;
//
//Parser p = new Parser;
//p.setLine(s);
//int a = p.eatInt();
//p.eatWhitespace();
//String b = p.eatString(' ');
//p.eatWhitespace();
//double d = p.eatDouble();