/*
 * Copyright (C) 2007-2014 by Brett Alistair Kromkamp <brett@polishedcode.com>.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class Node {

    private String identifier;
    private ArrayList<String> children;

    // Constructor
    public Node(String identifier) {
        this.identifier = identifier;
        children = new ArrayList<String>();
    }

    // Properties
    public String getIdentifier() {
        return identifier;
    }

    public ArrayList<String> getChildren() {
        return children;
    }

    // Public interface
    public void addChild(String identifier) {
        children.add(identifier);
    }
}

class BreadthFirstTreeIterator implements Iterator<Node> {

    private static final int ROOT = 0;

    private LinkedList<Node> list;
    private HashMap<Integer, ArrayList<String>> levels;

    public BreadthFirstTreeIterator(HashMap<String, Node> tree, String identifier) {
        list = new LinkedList<Node>();
        levels = new HashMap<Integer, ArrayList<String>>();

        if (tree.containsKey(identifier)) {
            this.buildList(tree, identifier, ROOT);

            for (Map.Entry<Integer, ArrayList<String>> entry : levels.entrySet()) {
                for (String child : entry.getValue()) {
                    list.add(tree.get(child));
                }
            }
        }
    }

    private void buildList(HashMap<String, Node> tree, String identifier, int level) {
        if (level == ROOT) {
            list.add(tree.get(identifier));
        }

        ArrayList<String> children = tree.get(identifier).getChildren();

        if (!levels.containsKey(level)) {
            levels.put(level, new ArrayList<String>());
        }
        for (String child : children) {
            levels.get(level).add(child);

            // Recursive call
            this.buildList(tree, child, level + 1);
        }
    }

    @Override
    public boolean hasNext() {
        return !list.isEmpty();
    }

    @Override
    public Node next() {
        return list.poll();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}


class DepthFirstTreeIterator implements Iterator<Node> {
    private LinkedList<Node> list;

    public DepthFirstTreeIterator(HashMap<String, Node> tree, String identifier) {
        list = new LinkedList<Node>();

        if (tree.containsKey(identifier)) {
            this.buildList(tree, identifier);
        }
    }

    private void buildList(HashMap<String, Node> tree, String identifier) {
        list.add(tree.get(identifier));
        ArrayList<String> children = tree.get(identifier).getChildren();
        for (String child : children) {

            // Recursive call
            this.buildList(tree, child);
        }
    }

    @Override
    public boolean hasNext() {
        return !list.isEmpty();
    }

    @Override
    public Node next() {
        return list.poll();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

enum TraversalStrategy {
    DEPTH_FIRST,
    BREADTH_FIRST
}


public class Tree {

   private final static int ROOT = 0;

   private HashMap<String, Node> nodes;
   private TraversalStrategy traversalStrategy;

   // Constructors
   public Tree() {
       this(TraversalStrategy.DEPTH_FIRST);
   }

   public Tree(TraversalStrategy traversalStrategy) {
       this.nodes = new HashMap<String, Node>();
       this.traversalStrategy = traversalStrategy;
   }

   // Properties
   public HashMap<String, Node> getNodes() {
       return nodes;
   }

   public TraversalStrategy getTraversalStrategy() {
       return traversalStrategy;
   }

   public void setTraversalStrategy(TraversalStrategy traversalStrategy) {
       this.traversalStrategy = traversalStrategy;
   }

   // Public interface
   public Node addNode(String identifier) {
       return this.addNode(identifier, null);
   }

   public Node addNode(String identifier, String parent) {
       Node node = new Node(identifier);
       nodes.put(identifier, node);

       if (parent != null) {
           nodes.get(parent).addChild(identifier);
       }

       return node;
   }

   public void display(String identifier) {
       this.display(identifier, ROOT);
   }

   public void display(String identifier, int depth) {
       ArrayList<String> children = nodes.get(identifier).getChildren();

       if (depth == ROOT) {
           System.out.println(nodes.get(identifier).getIdentifier());
       } else {
           String tabs = String.format("%0" + depth + "d", 0).replace("0", "    "); // 4 spaces
           System.out.println(tabs + nodes.get(identifier).getIdentifier());
       }
       depth++;
       for (String child : children) {

           // Recursive call
           this.display(child, depth);
       }
   }

   public Iterator<Node> iterator(String identifier) {
       return this.iterator(identifier, traversalStrategy);
   }

   public Iterator<Node> iterator(String identifier, TraversalStrategy traversalStrategy) {
       return traversalStrategy == TraversalStrategy.BREADTH_FIRST ?
               new BreadthFirstTreeIterator(nodes, identifier) :
               new DepthFirstTreeIterator(nodes, identifier);
   }
   
   
   public static void main(String[] args) {

       Tree tree = new Tree();

       /*
        * The second parameter for the addNode method is the identifier
        * for the node's parent. In the case of the root node, either
        * null is provided or no second parameter is provided.
        */
       tree.addNode("Harry");
       tree.addNode("Jane", "Harry");
       tree.addNode("Bill", "Harry");
       tree.addNode("Joe", "Jane");
       tree.addNode("Diane", "Jane");
       tree.addNode("George", "Diane");
       tree.addNode("Mary", "Diane");
       tree.addNode("Jill", "George");
       tree.addNode("Carol", "Jill");
       tree.addNode("Grace", "Bill");
       tree.addNode("Mark", "Jane");

       tree.display("Harry");

       System.out.println("\n***** DEPTH-FIRST ITERATION *****");

       // Default traversal strategy is 'depth-first'
       Iterator<Node> depthIterator = tree.iterator("Harry");

       while (depthIterator.hasNext()) {
           Node node = depthIterator.next();
           System.out.println(node.getIdentifier());
       }

       System.out.println("\n***** BREADTH-FIRST ITERATION *****");

       Iterator<Node> breadthIterator = tree.iterator("Harry", TraversalStrategy.BREADTH_FIRST);

       while (breadthIterator.hasNext()) {
           Node node = breadthIterator.next();
           System.out.println(node.getIdentifier());
       }
   }
   
}
